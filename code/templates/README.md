
## Instructions

* look at the `OrderedVector` code
* compile and run `test.cpp`. See the ordering
* modify `test.cpp` and reuse `OrderedVector` with `Complex`
* improve `OrderedVector` to template the ordering
* test reverse ordering of strings (from the last letter)
* test manhattan order with complex type
* check the implementation of `Complex`
* try ordering complex of complex