
# Hello World !

This example should help to check that your machine is well installed.

## g++ & make

Try:
```
make
./hello
```

## valgrind & callgrind & X11

Try:
```
valgrind --tool=callgrind ./hello
kcachegrind
```

## cppcheck

Try:
```
cppcheck .
```
